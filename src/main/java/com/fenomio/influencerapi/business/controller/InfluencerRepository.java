package com.fenomio.influencerapi.business.controller;

import com.fenomio.influencerapi.business.entity.Influencer;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

import java.util.Optional;

@SuppressWarnings("CdiManagedBeanInconsistencyInspection")
@Repository
public interface InfluencerRepository extends EntityRepository<Influencer, Long> {

    Optional<Influencer> findByUsername(String username);

}
