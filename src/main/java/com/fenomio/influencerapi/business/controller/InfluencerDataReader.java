package com.fenomio.influencerapi.business.controller;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.instagram.IgUser;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;

@RequestScoped
public class InfluencerDataReader {

    private FacebookClient client;

    @PostConstruct
    void init() {
        FacebookClient.AccessToken accessToken = new DefaultFacebookClient(Version.LATEST)
                .obtainAppAccessToken("350974435781457", "e6241cc4c061d1b97cc3a2d3621e61d0");
        client = new DefaultFacebookClient(accessToken.getAccessToken(), Version.LATEST);
    }

    public IgUser readInfluencerDetails(String username) {
        return client.fetchObject(username, IgUser.class, Parameter.with("metadata", 1));
    }

}
